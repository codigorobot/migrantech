import {Component, OnDestroy, OnInit} from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager, JhiLanguageService, JhiParseLinks} from 'ng-jhipster';

import {
    Account, LoginModalService, Principal, LOGIN_ALREADY_USED_TYPE, EMAIL_ALREADY_USED_TYPE,
    ResponseWrapper, ITEMS_PER_PAGE
} from '../shared';
import {Node} from '../entities/node';
import {TimelineService} from './timeline.service';
import {ActivatedRoute} from '@angular/router';
import {RequestOptions} from '@angular/http';
import {Subscription} from 'rxjs/Rx';

@Component({
    selector: 'jhi-timeline',
    templateUrl: './timeline.component.html',
    styleUrls: [
        'timeline.scss'
    ]

})
export class TimelineComponent implements OnInit, OnDestroy  {
    account: Account;
    modalRef: NgbModalRef;
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    success: boolean;
    predicate: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    reverse: any;
    routeData: any;
    page: any;
    isLoading: boolean;
    previousPage: any;
    nodes: Node[];
    activeRequest: any;
    activeFilters: any;
    showCommentAreaFlag: boolean;
    currentUser: any;
    eventSubscriber: Subscription;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private timelineService: TimelineService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private activatedRoute: ActivatedRoute,
        private eventManager: JhiEventManager,
        private languageService: JhiLanguageService,
    ) {
        this.principal.identity().then((account) => {
            this.currentUser = account;
        });
    }

    ngOnInit() {
        this.nodes = [];
        this.activeFilters = {};
        this.activeRequest = {};
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.success = false;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.showCommentAreaFlag = false;
        this.loadAll();
        this.eventSubscriber = this.eventManager.subscribe('nodeListModification', (response) => this.reset());
    }
    changeFilter(filter: any) {
        this.nodes = [];
        this.page = 0;
        this.activeRequest = filter;
        this.loadAll();
    }
    loadAll() {
        this.isLoading = true;
        this.activeRequest.page = this.page;
        this.activeRequest.size = this.itemsPerPage;
        this.activeRequest.sort = this.sort();
        this.timelineService.queryOptions(this.activeRequest).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page) {
        this.page = page;
        this.loadAll();
    }
    reset() {
        this.page = 0;
        this.nodes = [];
        this.loadAll();
    }
    sort() {
        const result = ['id' + ',' + ('desc')];
        return result;
    }
    parse(input: string) {
        return JSON.parse(input);
    }
    login() {
        this.modalRef = this.loginModalService.open();
    }
    register() {

    }
    like(node: any) {
        node.like = ! node.like;
    }
    showCommentArea(node: any) {
        node.showCommentAreaFlag = ! node.showCommentAreaFlag;
    }
    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            data[i].body = JSON.parse(data[i].body);
            if (!data[i].body.comments) {
                data[i].like = false;
                data[i].showCommentAreaFlag = false;
                data[i].body.comments = [];
            }
            this.nodes.push(data[i]);
        }
        console.log(this.nodes);
        this.isLoading = false;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
        this.isLoading = false;
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

}
