import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MigrantechSharedModule } from '../shared';
import { TIMELINE_ROUTE, TimelineComponent } from './';
import {TimelineService} from './timeline.service';
import { AgmCoreModule } from '@agm/core';
import {CommentComponent} from '../layouts/comment/comment.component';

@NgModule({
    imports: [
        MigrantechSharedModule,
        RouterModule.forChild([ TIMELINE_ROUTE ])
    ],
    declarations: [
        TimelineComponent,
        CommentComponent
    ],
    entryComponents: [
    ],
    providers: [
        TimelineService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrantechTimelineModule {}
