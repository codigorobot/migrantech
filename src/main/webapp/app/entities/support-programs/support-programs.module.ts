import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MigrantechSharedModule } from '../../shared';
import { NodeService } from '../node';
import {
    SupportProgramsPopupService,
    SupportProgramsComponent,
    SupportProgramsDetailComponent,
    SupportProgramsDialogComponent,
    SupportProgramsPopupComponent,
    SupportProgramsDeletePopupComponent,
    SupportProgramsDeleteDialogComponent,
    supportProgramsRoute,
    supportProgramsPopupRoute,
} from './';

const ENTITY_STATES = [
    ...supportProgramsRoute,
    ...supportProgramsPopupRoute,
];

@NgModule({
    imports: [
        MigrantechSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SupportProgramsComponent,
        SupportProgramsDetailComponent,
        SupportProgramsDialogComponent,
        SupportProgramsDeleteDialogComponent,
        SupportProgramsPopupComponent,
        SupportProgramsDeletePopupComponent,
    ],
    entryComponents: [
        SupportProgramsComponent,
        SupportProgramsDialogComponent,
        SupportProgramsPopupComponent,
        SupportProgramsDeleteDialogComponent,
        SupportProgramsDeletePopupComponent,
    ],
    providers: [
        NodeService,
        SupportProgramsPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrantechSupportProgramsModule {}
