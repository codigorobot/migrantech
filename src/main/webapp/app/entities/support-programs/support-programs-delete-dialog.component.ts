import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Node } from '../node';
import { SupportProgramsPopupService } from './support-programs-popup.service';
import { NodeService } from '../node';

@Component({
    selector: 'jhi-support-programs-delete-dialog',
    templateUrl: './support-programs-delete-dialog.component.html'
})
export class SupportProgramsDeleteDialogComponent {

    node: Node;

    constructor(
        private nodeService: NodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.nodeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'nodeListModification',
                content: 'Deleted an node'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-support-programs-delete-popup',
    template: ''
})
export class SupportProgramsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private nodePopupService: SupportProgramsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.nodePopupService
                .open(SupportProgramsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
