import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import {Node, TypeNode} from '../node';
import { SupportProgramsPopupService } from './support-programs-popup.service';
import { NodeService } from '../node';
import { Tag, TagService } from '../tag';
import { Place, PlaceService } from '../place';
import { ResponseWrapper } from '../../shared';
import { SupportProgram } from './support-programs.model';

@Component({
    selector: 'jhi-support-programs-dialog',
    templateUrl: './support-programs-dialog.component.html'
})
export class SupportProgramsDialogComponent implements OnInit {

    node: Node;
    isSaving: boolean;
    tagsExternal = [];
    areaExternal: any;
    placeExternal: any;
    tags: Tag[];
    places: Place[];
    jsonBody: SupportProgram;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private nodeService: NodeService,
        private tagService: TagService,
        private placeService: PlaceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {

        this.isSaving = false;
        this.tagsExternal = this.node.tags;
        this.areaExternal = this.node.influenceArea;
        this.node.type = TypeNode.ProgramaApoyo;
        if (this.node && this.node.body) {
            console.log(JSON.parse(this.node.body));
            this.jsonBody = JSON.parse(this.node.body);
        } else {
            this.jsonBody = new SupportProgram;
        }

        if (this.node.places === undefined || this.node.places.length === 0 ) {
            this.placeExternal = {id: undefined, name: undefined, address: undefined, location: undefined};
         }else {
            this.placeExternal = this.node.places[0];
        }

        this.tagService.query()
            .subscribe((res: ResponseWrapper) => { this.tags = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.placeService.query()
            .subscribe((res: ResponseWrapper) => { this.places = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.node.tags = this.tagsExternal;
        this.node.body = JSON.stringify(this.jsonBody);
        this.node.influenceArea = JSON.stringify(this.areaExternal);
        if (this.placeExternal.name !== undefined && this.placeExternal.name !== '' ) {
            this.node.places = [this.placeExternal];
        }else {
            this.node.places = undefined;
        }

        this.isSaving = true;
        if (this.node.id !== undefined) {
            this.subscribeToSaveResponse(
                this.nodeService.update(this.node));
        } else {
            this.subscribeToSaveResponse(
                this.nodeService.create(this.node));
        }
    }

    private subscribeToSaveResponse(result: Observable<Node>) {
        result.subscribe((res: Node) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Node) {
        this.eventManager.broadcast({ name: 'nodeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTagById(index: number, item: Tag) {
        return item.id;
    }

    trackPlaceById(index: number, item: Place) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }
}

@Component({
    selector: 'jhi-support-programs-popup',
    template: ''
})
export class SupportProgramsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private nodePopupService: SupportProgramsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.nodePopupService
                    .open(SupportProgramsDialogComponent as Component, params['id']);
            } else {
                this.nodePopupService
                    .open(SupportProgramsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
