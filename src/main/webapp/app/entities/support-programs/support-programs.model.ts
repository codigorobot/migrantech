import { BaseEntity } from './../../shared';

export class Url {
    constructor(
        public title?: string,
        public src?: string,
    ) {
        this.title = title ? title : null;
        this.src = src ? src : null;
    }
}
export class SupportProgram {
    constructor(
        public body?: string,
        public dependence?: string,
        public directedTo?: string,
        public fileContentType?: string,
        public file?: any,
        public url?:  Url,
        public validityTo?: Date,
    ) {
        this.body = body ? body : null;
        this.url = url ? url : new Url;
    }
}
