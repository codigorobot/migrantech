import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { SupportProgramsComponent } from './support-programs.component';
import { SupportProgramsDetailComponent } from './support-programs-detail.component';
import { SupportProgramsPopupComponent } from './support-programs-dialog.component';
import { SupportProgramsDeletePopupComponent } from './support-programs-delete-dialog.component';

export const supportProgramsRoute: Routes = [
    {
        path: 'support-program',
        component: SupportProgramsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'migrantechApp.support-program.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'support-program/:id',
        component: SupportProgramsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'migrantechApp.support-program.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const supportProgramsPopupRoute: Routes = [
    {
        path: 'support-program-new',
        component: SupportProgramsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'migrantechApp.support-program.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'support-program/:id/edit',
        component: SupportProgramsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'migrantechApp.support-program.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'support-program/:id/delete',
        component: SupportProgramsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'migrantechApp.support-program.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
