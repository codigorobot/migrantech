import { BaseEntity } from './../../shared';

export class Url {
    constructor(
        public title?: string,
        public src?: string,
    ) {
        this.title = title ? title : null;
        this.src = src ? src : null;
    }
}
export class Blog {
    constructor(
        public body?: string,
        public fileContentType?: string,
        public file?: any,
        public url?:  Url,
    ) {
        this.body = body ? body : null;
        this.url = url ? url : new Url;
    }
}
