import { BaseEntity } from './../../shared';

export class Place implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public location?: string,
        public nodes?: BaseEntity[],
    ) {
    }
}
