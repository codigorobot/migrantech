import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import {Node, TypeNode} from './node.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class NodeService {

    private resourceUrl = SERVER_API_URL + 'api/nodes';

    constructor(private http: Http) { }

    create(node: Node): Observable<Node> {
        const copy = this.convert(node);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(node: Node): Observable<Node> {
        const copy = this.convert(node);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Node> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    findEdoByName(name: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/edo/${name}`)
            .map((res: Response) => this.convertResponse(res));
    }

    findMunByName(name: string, cve: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/mun/${name}/${cve}`)
            .map((res: Response) => this.convertResponse(res));
    }

    findMunByGid(gid: number): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/mun/${gid}`)
            .map((res: Response) => this.convertResponse(res));
    }

    findAllCurrentUserNodesByType(type: string, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/by-type/${type}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllStates(): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/all-states`)
            .map((res: Response) => this.convertResponseAsString(res));
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponseAsString(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Node.
     */
    private convertItemFromServer(json: any): Node {
        const entity: Node = Object.assign(new Node(), json);
        return entity;
    }

    /**
     * Convert a Node to a JSON which can be sent to the server.
     */
    private convert(node: Node): Node {
        const copy: Node = Object.assign({}, node);
        return copy;
    }
}
