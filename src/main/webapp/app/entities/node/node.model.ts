import { BaseEntity } from './../../shared';

export const enum Topic {
    'Educacion',
    'Trabajo',
    'Emprendurismo'
}

export const enum TypeNode {
    'Pregunta',
    'Blog',
    'ProgramaApoyo',
    'Notas',
    'BolsaTrabajo',
    'Eventos',
    'Convocatorias',
    'OficinaAtencion',
}

export class Node implements BaseEntity {
    constructor(
        public id?: number,
        public title?: string,
        public body?: string,
        public influenceArea?: string,
        public topic?: Topic,
        public type?: TypeNode,
        public fileContentType?: string,
        public file?: any,
        public tags?: BaseEntity[],
        public places?: BaseEntity[],
    ) {
    }
}
