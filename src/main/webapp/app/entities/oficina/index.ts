export * from './oficina-popup.service';
export * from './oficina-dialog.component';
export * from './oficina-delete-dialog.component';
export * from './oficina-detail.component';
export * from './oficina.component';
export * from './oficina.route';
