import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MigrantechSharedModule } from '../../shared';
import { NodeService } from '../node';
import {
    OficinaPopupService,
    OficinaComponent,
    OficinaDetailComponent,
    OficinaDialogComponent,
    OficinaPopupComponent,
    OficinaDeletePopupComponent,
    OficinaDeleteDialogComponent,
    oficinaRoute,
    oficinaPopupRoute,
} from './';

const ENTITY_STATES = [
    ...oficinaRoute,
    ...oficinaPopupRoute,
];

@NgModule({
    imports: [
        MigrantechSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        OficinaComponent,
        OficinaDetailComponent,
        OficinaDialogComponent,
        OficinaDeleteDialogComponent,
        OficinaPopupComponent,
        OficinaDeletePopupComponent,
    ],
    entryComponents: [
        OficinaComponent,
        OficinaDialogComponent,
        OficinaPopupComponent,
        OficinaDeleteDialogComponent,
        OficinaDeletePopupComponent,
    ],
    providers: [
        NodeService,
        OficinaPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrantechOficinaModule {}
