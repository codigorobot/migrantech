import { BaseEntity } from './../../shared';

export class Tag implements BaseEntity {
    constructor(
        public id?: number,
        public title?: string,
        public weight?: number,
        public nodes?: BaseEntity[],
    ) {
    }
}
