import { BaseEntity } from './../../shared';

export class Event {
    constructor(
        public body?: string,
        public fileContentType?: string,
        public file?: any,
        public date?: Date,
    ) {
        this.body = body ? body : null;
    }
}
