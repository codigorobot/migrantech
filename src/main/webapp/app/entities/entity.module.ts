import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MigrantechTagModule } from './tag/tag.module';
import { MigrantechNodeModule } from './node/node.module';
import { MigrantechBlogModule } from './blog/blog.module';
import { MigrantechSupportProgramsModule } from './support-programs/support-programs.module';
import { MigrantechEventModule } from './event/event.module';
import { MigrantechPlaceModule } from './place/place.module';
import { MigrantechExperienceModule } from './experience/experience.module';
import { MigrantechQuestionModule } from './question/question.module';
import { MigrantechOficinaModule } from './oficina/oficina.module';
import { MigrantechPositionModule } from './position/position.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        MigrantechTagModule,
        MigrantechNodeModule,
        MigrantechBlogModule,
        MigrantechSupportProgramsModule,
        MigrantechEventModule,
        MigrantechPlaceModule,
        MigrantechExperienceModule,
        MigrantechQuestionModule,
        MigrantechOficinaModule,
        MigrantechPositionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrantechEntityModule {}
