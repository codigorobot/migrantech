import {ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot} from '@angular/router';

import { LinkMigrantechComponent } from './';
import {JhiPaginationUtil} from 'ng-jhipster';
import {Injectable} from '@angular/core';
import {TagResolvePagingParams} from '../entities/tag/tag.route';
import {UserRouteAccessService} from '../shared';

@Injectable()
export class LinkMigrantechResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const TIMELINE_ROUTE: Route = {
    path: 'link',
    component: LinkMigrantechComponent,
    resolve: {
        'pagingParams': TagResolvePagingParams
    },
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_USER'],
        pageTitle: 'home.title'
    },
    canActivate: [UserRouteAccessService]
};
