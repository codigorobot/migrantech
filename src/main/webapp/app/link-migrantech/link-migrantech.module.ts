import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MigrantechSharedModule } from '../shared';
import { TIMELINE_ROUTE, LinkMigrantechComponent } from './';
import {AppMenuComponent, FiltersComponent, OperationsComponent} from '../layouts';
import {ProfileBlockComponent} from '../layouts/profileblock/profile-block.component';
import {TimelineMapComponent} from '../layouts/timeline-map/timeline-map.component';
import {LinkMigrantechService} from './link-migrantech.service';
import { AgmCoreModule } from '@agm/core';
import {CommentComponent} from '../layouts/comment/comment.component';

@NgModule({
    imports: [
        MigrantechSharedModule,
        RouterModule.forChild([ TIMELINE_ROUTE ]),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC4F7DRotU1JniCKxzZBRwvt6UEsQ5rO0o'
        }),
    ],
    declarations: [
        LinkMigrantechComponent
    ],
    entryComponents: [
    ],
    providers: [
        LinkMigrantechService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrantechLinkModule {}
