import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../app.constants';
import {ResponseWrapper, createRequestOption, User} from '../shared';

@Injectable()
export class LinkMigrantechService {

    private resourceUrl = SERVER_API_URL + 'api/sponsor/query';

    constructor(private http: Http) { }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    queryOptions(req?: any): Observable<ResponseWrapper> {
        const options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            const params: URLSearchParams = new URLSearchParams();
            for (const prop in req) {
                if (req.hasOwnProperty(prop)) {
                    if ( prop === 'sort') {
                        params.paramsMap.set('sort', req.sort);
                    } else if ( prop === 'query' ) {
                        params.set('query', req.query);
                    } else {
                        params.set(prop, req[prop]);
                    }
                }
            }
            options.params = params;
        }
        console.log(options);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Tag.
     */
    private convertItemFromServer(json: any):  User {
        const entity: User = Object.assign(new User(), json);
        return entity;
    }

    /**
     * Convert a Tag to a JSON which can be sent to the server.
     */
    private convert(tag: User): User {
        const copy: User = Object.assign({}, tag);
        return copy;
    }
}
