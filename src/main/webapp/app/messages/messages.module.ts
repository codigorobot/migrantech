import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MigrantechSharedModule } from '../shared';

import { MESSAGES_ROUTE, MessagesComponent } from './';

@NgModule({
    imports: [
        MigrantechSharedModule,
        RouterModule.forChild([ MESSAGES_ROUTE ])
    ],
    declarations: [
        MessagesComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrantechMessagesModule {}
