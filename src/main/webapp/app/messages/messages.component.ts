import { Component, OnInit, HostListener,  ElementRef, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';
import { Account, LoginModalService, Principal, LOGIN_ALREADY_USED_TYPE, EMAIL_ALREADY_USED_TYPE } from '../shared';

@Component({
    selector: 'jhi-messages',
    templateUrl: './messages.component.html',
    styleUrls: [
        'messages.scss'
    ],
})
export class MessagesComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    success: boolean;
    selectedUser: any;
    actualUser: any;
    messages: any;
    actualText: any;
    innerHeight: string;
    heightWindow: any;
    users: any;
    @ViewChild('scrollMe') private myScrollContainer: ElementRef;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private languageService: JhiLanguageService,
    ) {
              this.heightWindow = window.screen.height - 170;
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;

            this.actualUser = {
                id: account.id,
                image: 'http://goo.gl/xt5Vdt',
                name: account.login,
                type: 'Sponsor'
            };

            if (account.file !== null) {
                this.actualUser.image = 'data:' + account.fileContentType + ';base64,' + account.file;
            }

            this.messages = [{user: this.selectedUser,
                      message: 'Hola Ludwig, ¿Cómo estás?',
                      align: 'left',
                      },
                      {user: this.actualUser,
                      message: 'Hola Alex. ¿bien y tú?',
                      align: 'right',
                      },
                      {user: this.selectedUser,
                      message: 'Bien Gracias!',
                      align: 'left',
                 }];
        });

        this.users = [{
            id: 2,
            image: 'http://goo.gl/49xhrA',
            name: 'Ludwig Rubio',
            type: 'Sponsor',
        }, {
            id: 3,
            image: 'https://goo.gl/ZCMVzK',
            name: 'María Fernanda',
            type: 'Sponsor'
        }, {
            id: 4,
            image: 'http://goo.gl/uPUjf6',
            name: 'Alonos Ruiz',
            type: 'Usuario'
        }, {
            id: 5,
            image: 'https://goo.gl/5GAcyj',
            name: 'Ileana Topete',
            type: 'Usuario'
        },
        {
            id: 6,
            image: 'https://goo.gl/dLcr4a',
            name: 'José María Ruvalcaba',
            type: 'Sponsor'
        }];

        this.selectedUser = this.users[0];
        this.actualText = '';
        this.success = false;
        this.actualUser = {};
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    sendMessage() {
         if (this.actualText !== '') {
            const object = {
                user: this.actualUser,
                message: this.actualText,
                align: 'right',
            };
           this.messages.push(object);
           this.actualText = '';
           this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
       this.heightWindow = event.target.innerHeight;
    }

    changeChat(selectedUser) {
        this.selectedUser = selectedUser;
        this.messages.forEach((message) => {

            if (message.align === 'right') {
                message.user = this.actualUser;
            } else {
             message.user =  selectedUser;
            }
        });
    }
}
