import { Route } from '@angular/router';

import { MessagesComponent } from './';

export const MESSAGES_ROUTE: Route = {
    path: 'messages',
    component: MessagesComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
