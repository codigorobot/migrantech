import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {AppMenuComponent} from './app-menu.component';

@NgModule({
    imports: [
        RouterModule
    ],
    declarations: [
        AppMenuComponent
    ],
    exports: [ AppMenuComponent ]
})
export class MigrantechAppMenuModule {}
