import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Principal, User} from '../../shared';
import {Node, NodeService} from '../../entities/node';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'jhi-comments',
    styleUrls: [
        'comment.scss'
    ],
    template: `
        <div class="comment mb-2 row" *ngFor="let comment of comments;">
            <div class="comment-avatar col-md-1 col-sm-1 text-center pr-1">
                <a href="">
                    <img *ngIf="comment.user && comment.user.file === null"
                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAn0lEQVR42u3RQREAAAQ
AMPpJ4qn/Qw3ntgrL7qngjBQiBCFCECIEIUIQIkSIEIQIQYgQhAhBiBCEIEQIQoQgRAhChCAEIUIQIgQhQhAiBCEIEYIQIQgRghAhCEGIEIQIQYgQhAhBCE
KEIEQIQoQgRAhCECIEIUIQIgQhQhCCECEIEYIQIQgRghCECEGIEIQIQYgQhAgRIgQhQhAiBCHfLZ18nW0We6NFAAAAAElFTkSuQmCC" class="mx-auto rounded-circle img-fluid"/>
                    <img *ngIf="comment.user && comment.user.file !== null"
                         src="{{'data:' + comment.user.fileContentType + ';base64,' + comment.user.file}}"
                         class="mx-auto rounded-circle img-fluid"/>
                </a>
            </div>
            <div class="comment-content col-md-11 col-sm-11">
                <h6 class="small comment-meta"><strong>{{ comment.user.login }}</strong> -
                    {{ comment.date | date:'short' }}</h6>
                <div class="comment-body">
                    <p>
                        {{ comment.message }}
                        <br>
                        <a (click)="comment.showCommentArea()" class="text-right small"><i class="ion-reply"></i>Replicar</a>
                    </p>
                </div>
                <div class="comment-reply-list">
                    <jhi-comments [originalNode]="originalNode" [level]="1" [comments]="comment.comments"
                                  [currentUser]="currentUser"></jhi-comments>
                </div>
                <div class="comment-reply-area" *ngIf="comment.reply">
                    <textarea [(ngModel)]="comment.replyMessage" class="form-control" cols="4"></textarea>
                    <div class="text-right">
                        <button (click)="comment.comment(currentUser, originalNode, nodeService)" class="btn btn-sm btn-secondary">Enviar mensaje
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div *ngIf="level == 0">
            <textarea [(ngModel)]="message" class="form-control" cols="4"></textarea>
            <div class="text-right">
                <button (click)="comment()" class="btn btn-sm btn-secondary">Enviar mensaje</button>
            </div>
        </div>
    `
})

export class CommentComponent implements OnInit {

    @Input() comments: CommentStructure[];
    @Input() level: number;
    public message: string;
    @Input() currentUser: any;
    @Input() originalNode: Node;
    saveComments() {
        const node = { ...this.originalNode };
        node.body = JSON.stringify(node.body);
        this.subscribeToSaveResponse(
            this.nodeService.update(node));
    }
    constructor(private nodeService: NodeService) {}
    comment() {
        const comment = new CommentStructure();
        comment.message = this.message;
        comment.user = this.currentUser;
        this.comments.push(comment);
        this.message = '';
        this.saveComments();
    }
    ngOnInit() {
        this.message = '';
        if (! this.comments) {
            this.comments = [];
        } else {
            this.comments.forEach(( value: CommentStructure, index ) => {
               let comment: CommentStructure = new CommentStructure();
               comment = Object.assign(comment, value);
               this.comments[index] = comment;
            });
        }
    }
    private subscribeToSaveResponse(result: Observable<Node>) {
        result.subscribe((res: Node) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Node) {
    }

    private onSaveError() {
    }
}

export class CommentStructure {
    public reply: boolean;
    public user: any;
    public message: string;
    public replyMessage: string;
    public date: Date;
    public comments: CommentStructure[];
    constructor() {
        this.reply = false;
        this.replyMessage = '';
        this.date = new Date();
        this.comments = [];
    }
    showCommentArea() {
        this.reply = !this.reply;
    }
    saveComments(originalNode: Node, nodeService: NodeService) {
        const node = { ...originalNode };
        node.body = JSON.stringify(node.body);
        this.subscribeToSaveResponse(
            nodeService.update(node));
    }
    comment(currentUser: any, originalNode: Node, nodeService: NodeService) {
        const comment = new CommentStructure();
        comment.message = this.replyMessage;
        comment.user = currentUser;
        this.comments.push(comment);
        this.replyMessage = '';
        this.reply = false;
        this.saveComments(originalNode, nodeService);
    }
    private subscribeToSaveResponse(result: Observable<Node>) {
        result.subscribe((res: Node) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Node) {
    }

    private onSaveError() {
    }
}
