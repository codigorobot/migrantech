import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'jhi-operations',
    templateUrl: './operations.component.html',
    styleUrls: [
        'operations.scss'
    ],
})
export class OperationsComponent implements OnInit {
    @Input() activeFilters: Object;
    @Input() changeFilter: Function;
    @Input() showOperations: boolean;
    @Input() showMapButton: boolean;
    terms: String;
    search() {
        this.activeFilters['search'] = this.terms;
        this.changeFilter(this.activeFilters);
    }
    reset() {
        this.terms = '';
        this.activeFilters['search'] = '';
        this.changeFilter(this.activeFilters);
    }
    ngOnInit() {
        if (this.showOperations === null || this.showOperations === undefined) {
            this.showOperations = true;
        }
        if (this.showMapButton === null || this.showMapButton === undefined) {
            this.showMapButton = true;
        }
        this.terms = '';
    }
}
