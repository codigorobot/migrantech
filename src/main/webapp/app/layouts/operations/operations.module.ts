import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {OperationsComponent} from './operations.component';
import {MigrantechTimelineMapModule} from '../timeline-map/timeline-map.module';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        CommonModule,
        MigrantechTimelineMapModule
    ],
    declarations: [
        OperationsComponent
    ],
    exports: [ OperationsComponent ]
})
export class MigrantechOperationsModule {}
