import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {TimelineComponent} from '../../timeline/timeline.component';
import {TagService} from '../../entities/tag/tag.service';
import {ResponseWrapper, UserService} from '../../shared';
import {NodeService} from '../../entities/node';
import {Router} from '@angular/router';
import {ExperienceService} from '../../entities/experience';

@Component({
    selector: 'jhi-filters',
    templateUrl: './filters.component.html',
    styleUrls: [
        'filters.scss'
    ]
})
export class FiltersComponent implements OnInit {
    @Input() changeFilter: Function;
    @Input() activeFilters: Object;

    filters = [];

    constructor(private tagService: TagService, private nodeService: NodeService, private userService: UserService,
                private router: Router, private experienceService: ExperienceService) {}

    changeOption(option: any, key: string) {
        option.status = ! option.status;
        this.filters.forEach((filter: any) => {
            if (filter.key && filter.key === key) {
                this.activeFilters[key] = '';
                filter.elements.forEach((opt: any) => {
                   if (opt.status === true) {
                       if (this.activeFilters[key].length > 0) {
                           this.activeFilters[key] += ',';
                       }
                       this.activeFilters[key] += opt.key;
                   }
                });
                this.changeFilter(this.activeFilters);
            }
        });
    }
    ngOnInit() {
        switch (this.router.url) {
            case '/link':
                this.filters.push({
                    'title' : 'Secciones tematicas',
                    'key' : 'topics',
                    'elements' : [
                        {
                            'label' : 'Educación',
                            'key' : 'Educacion'
                        },
                        {
                            'label' : 'Trabajo',
                            'key' : 'Trabajo'
                        },
                        {
                            'label' : 'Emprendedurismo',
                            'key' : 'Emprendurismo'
                        },
                    ]
                });
                this.experienceService.query({
                    page: 0,
                    size: 100,
                    sort: ['name,asc'] }).subscribe(
                    (res: ResponseWrapper) =>  {
                        if (res.json.length > 0) {
                            const expFilter = {
                                'title' : 'Áreas de experiencia',
                                'key' : 'exp',
                                'elements' : []
                            };
                            res.json.forEach((value) => {
                                expFilter.elements.push({label: value.name, key: value.id});
                            });
                            this.filters.push(expFilter);
                        }
                    },
                    (res: ResponseWrapper) => {}
                );
                this.nodeService.getAllStates().subscribe(
                    (res: ResponseWrapper) =>  {
                        if (res.json.length > 0) {
                            const stateFilter = {
                                'title' : 'Lugar de origen',
                                'key' : 'origen',
                                'elements' : []
                            };
                            res.json.forEach((value: string) => {
                                stateFilter.elements.push({label: value, key: value});
                            });
                            this.filters.push(stateFilter);

                            const state1Filter = {
                                'title' : 'Residencia actual',
                                'key' : 'res',
                                'page': 'timeline',
                                'elements' : []
                            };
                            res.json.forEach((value: string) => {
                                state1Filter.elements.push({label: value, key: value});
                            });
                            this.filters.push(state1Filter);
                        }
                    },
                    (res: ResponseWrapper) => {}
                );
                break;
            case '/timeline':
                this.filters.push({
                    'title' : 'Secciones tematicas',
                    'key' : 'topics',
                    'elements' : [
                        {
                            'label' : 'Educación',
                            'key' : 'Educacion'
                        },
                        {
                            'label' : 'Trabajo',
                            'key' : 'Trabajo'
                        },
                        {
                            'label' : 'Emprendedurismo',
                            'key' : 'Emprendurismo'
                        },
                    ]
                });
                this.filters.push(
                    {
                        'title' : 'Tipo de información',
                        'key' : 'nodeTypes',
                        'elements' : [
                            {
                                'label' : 'Preguntas',
                                'key' : 'Pregunta'
                            },
                            {
                                'label' : 'Publicaciones en blogs',
                                'key' : 'Blog'
                            },
                            {
                                'label' : 'Eventos',
                                'key' : 'Eventos'
                            },
                            {
                                'label' : 'Convocatorias',
                                'key' : 'Convocatoria'
                            },
                            {
                                'label' : 'Posiciones de trabajo',
                                'key' : 'BolsaTabajo'
                            },
                            {
                                'label' : 'Programas de apoyo al migrante',
                                'key' : 'ProgramaApoyo'
                            },
                            {
                                'label' : 'Lugares / Oficinas de apoyo al migrante',
                                'key' : 'OficinaAtencion'
                            },
                        ]
                    });
                    this.nodeService.getAllStates().subscribe(
                        (res: ResponseWrapper) =>  {
                            if (res.json.length > 0) {
                                const stateFilter = {
                                    'title' : 'Ubicación',
                                    'key' : 'states',
                                    'page': 'timeline',
                                    'elements' : []
                                };
                                res.json.forEach((value: string) => {
                                    stateFilter.elements.push({label: value, key: value});
                                });
                                this.filters.push(stateFilter);
                            }
                        },
                        (res: ResponseWrapper) => {}
                    );
                    this.userService.query({
                        page: 0,
                        size: 100,
                        sort: ['login,asc'] }).subscribe(
                        (res: ResponseWrapper) =>  {
                            if (res.json.length > 0) {
                                const tagFilter = {
                                    'title' : 'Usuarios',
                                    'key' : 'users',
                                    'elements' : []
                                };
                                res.json.forEach((value) => {
                                    tagFilter.elements.push({label: value.login, key: value.login});
                                });
                                this.filters.push(tagFilter);
                            }
                        },
                        (res: ResponseWrapper) => {}
                    );
                break;
        }

        this.tagService.query({
            page: 0,
            size: 100,
            sort: ['title,asc'] }).subscribe(
            (res: ResponseWrapper) =>  {
                if (res.json.length > 0) {
                    const tagFilter = {
                        'title' : 'Tags',
                        'key' : 'tags',
                        'elements' : []
                    };
                    res.json.forEach((value) => {
                        tagFilter.elements.push({label: value.title, key: value.id});
                    });
                    this.filters.push(tagFilter);
                }
             },
            (res: ResponseWrapper) => {}
        );
    }
}
