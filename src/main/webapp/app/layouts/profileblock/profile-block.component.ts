import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {JhiLanguageHelper, LoginModalService, LoginService, Principal, StateStorageService} from '../../shared';
import {RouterLink} from '@angular/router';

@Component({
    selector: 'jhi-profile-block',
    templateUrl: './profile-block.component.html',
    styleUrls: [
        './profile-block.scss'
    ]
})
export class ProfileBlockComponent implements OnInit {

    account: any;

    constructor(
        private principal: Principal
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
            if (this.account && this.account.originArea && !this.account.originArea.nomEnt) {
                this.account.originArea = JSON.parse(this.account.originArea);
            }
            if (this.account && this.account.currentArea && !this.account.currentArea.nomEnt) {
                this.account.currentArea = JSON.parse(this.account.currentArea);
            }
        });
    }
}
