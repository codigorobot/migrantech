import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {ProfileBlockComponent} from './profile-block.component';
import {MigrantechTimelineMapModule} from '../timeline-map/timeline-map.module';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [
        RouterModule,
        CommonModule
    ],
    declarations: [
        ProfileBlockComponent
    ],
    exports: [ ProfileBlockComponent ]
})
export class MigrantechProfileBlockModule {}
