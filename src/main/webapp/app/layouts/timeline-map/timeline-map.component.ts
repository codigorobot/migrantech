import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {JhiLanguageHelper, LoginModalService, LoginService, Principal, StateStorageService} from '../../shared';
import * as screenfull from 'screenfull';
import {ResponseWrapper } from '../../shared';
import { Place } from '../../entities/place/place.model';
import { PlaceService } from '../../entities/place/place.service';

@Component({
    selector: 'jhi-timeline-map',
    templateUrl: './timeline-map.component.html',
    styleUrls: [
        './timeline-map.scss'
    ]
})
export class TimelineMapComponent implements OnInit {

    account: any;
    centerMap: any = {  lat: 23.2533197, lng: -108.3402586};
    places: Place[];
    iconsMap: any = [
    { img: 'aboriginal', description: 'Atención a la mujer migrante'},
    { img: 'medicine', description: 'Atención médica'},
    { img: 'barber', description: 'Atención de aseo personal'},
    { img: 'bus', description: 'Transporte público'},
    { img: 'cabin-2', description: 'Refugio para migrantes'},
    { img: 'coffee', description: 'Comida'},
    { img: 'communitycentre', description: 'Centros comunitarios'},
    { img: 'cramschool', description: 'Educación'},
    { img: 'fastfood', description: 'Comida gratuita'},
    { img: 'female-2', description: 'Apoyo a la mujer'},
    { img: 'laundromat', description: 'Apoyo en aseo de ropa'},
    { img: 'lodging_0star', description: 'Dormitorios para migrantes'},
    { img: 'office-building', description: 'Trabajo'},
    { img: 'schreibwaren_web', description: 'Cursos y educación'},
    { img: 'shower', description: 'Apoyo en aseo personal'},
    { img: 'telephone', description: 'Apoyo en comunicaciones'},
    { img: 'medicine', description: 'Atención médica'},
    { img: 'wifi', description: 'Servicios de comunicación'},
    { img: 'bus', description: 'Transporte público'},
    ];
    constructor(
        private principal: Principal,
        private placeService: PlaceService,
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.places = [];
        screenfull.onchange( function(this){
               if (screenfull.isFullscreen ) {
                    document.getElementById('map-wrapper').className = 'view';
                } else {
                    document.getElementById('map-wrapper').className = 'sr-only';
                }
        } );

        this.loadAll();
    }

    loadAll() {
        this.placeService.query({
               }).subscribe(
                (res: ResponseWrapper) => this.onSuccessPlaces(res.json, res.headers)
            );
    }

   private onSuccessPlaces(data, headers) {
        let lat = 0;
        let lng = 0;
        for (let i = 0; i < data.length; i++) {

            const coord = data[i].location.replace('POINT (', '').replace(')', '').split(' ');
            lng = parseFloat(coord[1]);
            lat = parseFloat(coord[0]);
            data[i].lng = lng;
            data[i].lat = lat;
            const url = require('../../../content/images/markers/' + this.randomImage());
            data[i].iconUrl = url;
            this.places.push(data[i]);
        }
    }

    public randomImage() {
        const rn = Math.random() * 10 + Math.random() * 10 - 1;
        return this.iconsMap[parseInt(rn + '', 10)].img + '.png';
    }

    clickToFullScreen() {
        const el = document.getElementById('map-wrapper');
        if (screenfull.enabled) {
                screenfull.request(el);
        }
    }

    getImage(index) {
       return require('../../../content/images/markers/' + this.iconsMap[index].img + '.png');
    }

}
