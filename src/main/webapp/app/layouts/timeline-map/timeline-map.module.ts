import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {TimelineMapComponent} from '../timeline-map/timeline-map.component';
import {Form, FormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC4F7DRotU1JniCKxzZBRwvt6UEsQ5rO0o'
        }),
    ],
    declarations: [
        TimelineMapComponent
    ],
    exports: [ TimelineMapComponent ]
})
export class MigrantechTimelineMapModule {}
