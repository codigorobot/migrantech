import './vendor.ts';

import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MigrantechSharedModule, UserRouteAccessService } from './shared';
import { MigrantechAppRoutingModule} from './app-routing.module';
import { MigrantechHomeModule } from './home/home.module';
import { MigrantechTimelineModule } from './timeline/timeline.module';
import { MigrantechMessagesModule } from './messages/messages.module';
import { MigrantechReportsModule } from './reports/reports.module';
import { MigrantechAdminModule } from './admin/admin.module';
import { AppMenuComponent } from './layouts/appmenu/app-menu.component';
import { MigrantechAccountModule } from './account/account.module';
import { MigrantechEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent,
} from './layouts';
import {MigrantechLinkModule} from './link-migrantech/link-migrantech.module';

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        MigrantechAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        MigrantechSharedModule,
        MigrantechHomeModule,
        MigrantechTimelineModule,
        MigrantechLinkModule,
        MigrantechMessagesModule,
        MigrantechReportsModule,
        MigrantechAdminModule,
        MigrantechAccountModule,
        MigrantechEntityModule,
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],

    bootstrap: [ JhiMainComponent ]
})
export class MigrantechAppModule {}
