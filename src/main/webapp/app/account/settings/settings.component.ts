import { Component, OnInit } from '@angular/core';
import {JhiDataUtils, JhiLanguageService} from 'ng-jhipster';

import {Principal, AccountService, JhiLanguageHelper, BaseEntity, UserService} from '../../shared';
import {IMultiSelectOption} from 'angular-4-dropdown-multiselect';

@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
    error: string;
    success: string;
    settingsAccount: any;
    authorities: any[];
    roles: IMultiSelectOption[] = [];
    languages: any[];

    constructor(
        private account: AccountService,
        private principal: Principal,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private dataUtils: JhiDataUtils,
        private userService: UserService
    ) {
    }

    ngOnInit() {
        this.userService.authorities().subscribe((authorities) => {
            this.authorities = authorities;
            this.authorities.forEach(( item ) => {
                if (this.settingsAccount.authorities.indexOf(item) !== -1 ||
                    ( item !== 'ROLE_ADMIN' )) {
                    this.roles.push({id: item, name: item.replace('ROLE_', '')});
                }
            });
        });
        this.principal.identity().then((account) => {
            this.settingsAccount = this.copyAccount(account);
            if (this.settingsAccount && this.settingsAccount.originArea && this.settingsAccount.originArea.nomEnt) {
                this.settingsAccount.originArea = JSON.stringify(this.settingsAccount.originArea);
            }
            if (this.settingsAccount && this.settingsAccount.currentArea && this.settingsAccount.currentArea.nomEnt) {
                this.settingsAccount.currentArea = JSON.stringify(this.settingsAccount.currentArea);
            }
        });
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

    }

    save() {

        this.settingsAccount.originArea = JSON.stringify(this.settingsAccount.originArea);
        this.settingsAccount.currentArea = JSON.stringify(this.settingsAccount.currentArea);

        this.account.save(this.settingsAccount).subscribe(() => {
            this.error = null;
            this.success = 'OK';
            this.principal.identity(true).then((account) => {
                this.settingsAccount = this.copyAccount(account);
            });
            this.languageService.getCurrent().then((current) => {
                if (this.settingsAccount.langKey !== current) {
                    this.languageService.changeLanguage(this.settingsAccount.langKey);
                }
            });
        }, () => {
            this.success = null;
            this.error = 'ERROR';
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl,
            file: account.file,
            fileContentType: account.fileContentType,
            authorities: account.authorities,
            nomInst: account.nomInst,
            showInList: account.showInList,
            originArea: account.originArea,
            currentArea: account.currentArea,
            experiences: account.experiences,
            topic: account.topic,
            bio: account.bio
        }
    }
}
