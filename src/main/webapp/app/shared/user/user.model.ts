import Any = jasmine.Any;
import {BaseEntity} from '../index';

export class User {
    public id?: any;
    public login?: string;
    public firstName?: string;
    public lastName?: string;
    public email?: string;
    public activated?: Boolean;
    public langKey?: string;
    public authorities?: any[];
    public createdBy?: string;
    public createdDate?: Date;
    public lastModifiedBy?: string;
    public lastModifiedDate?: Date;
    public password?: string;
    public fileContentType?: string;
    public file?: any;
    public nomInst?: string;
    public showInList?: Boolean;
    public originArea?: any;
    public currentArea?: any;
    public experiences?: BaseEntity[];
    public bio?: string;

    constructor(
        id?: any,
        login?: string,
        firstName?: string,
        lastName?: string,
        email?: string,
        activated?: Boolean,
        langKey?: string,
        authorities?: any[],
        createdBy?: string,
        createdDate?: Date,
        lastModifiedBy?: string,
        lastModifiedDate?: Date,
        password?: string,
        fileContentType?: string,
        file?: any,
        showInList?: Boolean,
        nomInst?: string,
        originArea?: any,
        currentArea?: any,
        experiences?: BaseEntity[],
        bio?: string
    ) {
        this.id = id ? id : null;
        this.login = login ? login : null;
        this.firstName = firstName ? firstName : null;
        this.lastName = lastName ? lastName : null;
        this.email = email ? email : null;
        this.activated = activated ? activated : false;
        this.langKey = langKey ? langKey : null;
        this.authorities = authorities ? authorities : null;
        this.createdBy = createdBy ? createdBy : null;
        this.createdDate = createdDate ? createdDate : null;
        this.lastModifiedBy = lastModifiedBy ? lastModifiedBy : null;
        this.lastModifiedDate = lastModifiedDate ? lastModifiedDate : null;
        this.password = password ? password : null;
        this.fileContentType = fileContentType ? fileContentType : null;
        this.file = file ? file : null;
        this.showInList = showInList ? showInList : false;
        this.nomInst = nomInst ? nomInst : null;
        this.originArea = originArea ? originArea : null;
        this.currentArea = currentArea ? currentArea : null;
        this.experiences = experiences ? experiences : null;
        this.bio = bio ? bio : null;
    }
}
