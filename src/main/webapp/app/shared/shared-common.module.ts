import { NgModule, LOCALE_ID } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { JhiTagsComponent } from './fields/tags/tags.component';
import { JhiAreamunComponent } from './fields/areamun/areamun.component';
import { TagInputModule } from 'ngx-chips';
import { JhiMapcaptureComponent } from './fields/mapcapture/mapcapture.component';
import { Ng2CompleterModule } from 'ng2-completer';
import { AgmCoreModule } from '@agm/core';
import {CommonModule} from '@angular/common';

import {
    MigrantechSharedLibsModule,
    JhiLanguageHelper,
    FindLanguageFromKeyPipe,
    JhiAlertComponent,
    JhiAlertErrorComponent,
} from './';
import {JhiExperiencesComponent} from './fields/experience/experience.component';

@NgModule({
    imports: [
        MigrantechSharedLibsModule,
        TagInputModule,
        Ng2CompleterModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC4F7DRotU1JniCKxzZBRwvt6UEsQ5rO0o'
         }),
        CommonModule,
    ],
    declarations: [
        FindLanguageFromKeyPipe,
        JhiAlertComponent,
        JhiAlertErrorComponent,
        JhiTagsComponent,
        JhiExperiencesComponent,
        JhiAreamunComponent,
        JhiMapcaptureComponent,
    ],
    providers: [
        JhiLanguageHelper,
        Title,
        {
            provide: LOCALE_ID,
            useValue: 'es'
        },
    ],
    exports: [
        MigrantechSharedLibsModule,
        FindLanguageFromKeyPipe,
        JhiAlertComponent,
        JhiAlertErrorComponent,
        JhiTagsComponent,
        JhiExperiencesComponent,
        JhiAreamunComponent,
        JhiMapcaptureComponent,
    ]
})
export class MigrantechSharedCommonModule {}
