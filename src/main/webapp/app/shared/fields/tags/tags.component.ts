import { Component, OnDestroy, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { filter, map } from 'rxjs/operators';
import { TagService } from '../../../entities/tag/tag.service';
import { ResponseWrapper } from '../../../shared';

@Component({
  selector: 'jhi-tags',
   templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class JhiTagsComponent implements OnInit, OnDestroy {

    options: any;
    autocompleteItems: any;
    queryListTags: any;
    tagsValue = [];
    @Output() tagsChange = new EventEmitter();

    @Input()
    get tags() {
        return this.tagsValue;
    }

    set tags(val) {
        this.tagsValue = val;
        this.tagsChange.emit(this.tagsValue);
   }

    constructor(private http: Http, private tagService: TagService, ) {
    }

    ngOnInit() {
        this.queryListTags = [];
        this.autocompleteItems = [];

        this.options = {
            placeholder: 'Añadir tags',
            secondaryPlaceholder: 'Añadir tags',
            maxItems: 10,
            autoComplete: true,
        }
    }

    ngOnDestroy() {
        this.tags = [];
    }

    public requestAutocompleteItems = (text: string): Observable<Response> => {
        return this.tagService.findByTitle(text);
    }

    public onAdding(tag): Observable<any> {
        console.log(this.tags);
        return of(tag)
            .pipe();
    }

    public onRemoving(tag): Observable<any> {
        return of(tag)
            .pipe();
    }

}
