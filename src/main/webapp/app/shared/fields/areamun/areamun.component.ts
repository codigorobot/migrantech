import { Component, OnDestroy, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TagService } from '../../../entities/tag/tag.service';
import { ResponseWrapper } from '../../../shared';
import { CompleterCmp, CompleterData, CompleterService, CompleterItem, RemoteData } from 'ng2-completer';

@Component({
  selector: 'jhi-areamun',
   templateUrl: './areamun.component.html',
  styleUrls: ['./areamun.component.css']
})
export class JhiAreamunComponent implements OnInit, OnDestroy {

    munValue: any;
    edo = '';
    munv = '';
    edoInner: any;
    dataRemote1: RemoteData;
    dataRemote2: RemoteData;
    state1 = false;
    state2 = false;

    @Output() munChange = new EventEmitter();
    @Input()
    get mun() {
        return this.munValue;
    }

    set mun(val) {
        this.munValue = val;
        this.munChange.emit(this.munValue);
    }

    constructor(private http: Http, completerService: CompleterService, ) {

        this.dataRemote1 = completerService.remote(
            'nomEnt',
            'nomEnt',
            'nomEnt');

        this.dataRemote1.urlFormater( (term) => {
            return '/api/nodes/edo/' + term;
        });

        this.dataRemote2 = completerService.remote(
            'nomMun',
            'nomMun',
            'nomMun');

        this.dataRemote2.urlFormater( (term) => {
            return '/api/nodes/mun/' + term + '/' + this.edoInner.cveEnt;
        });

    }

    ngOnInit() {
        this.edoInner = {};
        // Remove some general bugs
        try {
            if (this.mun !== undefined) {
                this.mun = JSON.parse(this.mun);
                this.edo = this.mun.nomEnt;
                this.munv = this.mun.nomMun;
                this.state1 = true;
                this.state2 = true;
            }
        } catch (e) {
            // Do nothing
        }
    }

    ngOnDestroy() {

    }

    public onStateSelected(selected: CompleterItem) {
        if (selected) {
            this.edoInner = selected.originalObject;
            this.state1 = true;
        } else {
            this.edoInner = {};
            this.state1 = false;
        }
    }

    public resetMun() {
        this.edo = '';
        this.state1 = false;
        this.state2 = false;
        this.munv = '';
        this.mun = {};
    }

    public onMunSelected(selected: CompleterItem) {

        if (selected) {
            this.mun = selected.originalObject;
            this.state2 = true;

        } else {
            this.mun = {};
            this.state2 = false;
        }
    }

}
