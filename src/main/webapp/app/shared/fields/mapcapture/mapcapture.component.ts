import { Component, OnDestroy, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TagService } from '../../../entities/tag/tag.service';
import { ResponseWrapper } from '../../../shared';
import { CompleterCmp, CompleterData, CompleterService, CompleterItem, RemoteData } from 'ng2-completer';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { PlaceService } from '../../../entities/place';
import { Place } from '../../../entities/place/place.model';
import { Node, NodeService } from '../../../entities/node';

@Component({
  selector: 'jhi-mapcapture',
   templateUrl: './mapcapture.component.html',
  styleUrls: ['./mapcapture.component.css']
})
export class JhiMapcaptureComponent implements OnInit, OnDestroy {

    centerMap: any = {  lat: 20.66, lng: -103.35};
    placeValue: any;
    isSaving: boolean;
    isTherePlace: any;

    @Output() placeChange = new EventEmitter();
    @Input()
    get place() {
        return this.placeValue;
    }

    set place(val) {
        this.placeValue = val;
        this.placeChange.emit(this.placeValue);
    }

    constructor(
        private placeService: PlaceService,
        private nodeService: NodeService,
        private eventManager: JhiEventManager,
        completerService: CompleterService,
        private http: Http
    ) {
    }

    ngOnInit() {

        this.isSaving = false;
        this.isTherePlace = { validate : false };
        this.isTherePlace.validate = (this.place === undefined) ? false : true;

        if ( this.place.id === undefined ) {
              this.place =  {id: undefined, name: undefined, address: undefined, location: undefined};
            } else {
            const values = this.place.location.replace('POINT (' , '').replace(')' , '').split(' ');
                this.centerMap.lat = + values[0];
                this.centerMap.lng = + values[1];
                this.centerMap.locationChosen = true;
            }
    }

    ngOnDestroy() {

    }

    private subscribeToSaveResponse(result: Observable<Place>) {
        result.subscribe((res: Place) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Place) {
        this.eventManager.broadcast({ name: 'placeListModification', content: 'OK'});
        this.isSaving = false;
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        console.log(error.message);
    }

    trackNodeById(index: number, item: Node) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    onChoseLocation(event) {
       this.place.location = 'POINT(' + event.coords.lat + ' ' + event.coords.lng + ')';
       this.centerMap.lat = event.coords.lat;
       this.centerMap.lng = event.coords.lng;
       this.centerMap.locationChosen = true;
    }

    onChangeIsTherePlace(validate) {
        if (validate) {
             this.isTherePlace.validate = false;
             this.centerMap.locationChosen = false;
             this.centerMap = {  lat: 20.66, lng: -103.35};
             this.place =  {id: undefined, name: undefined, address: undefined, location: undefined};
        } else {
             this.isTherePlace.validate = true;
        }
    }
}
