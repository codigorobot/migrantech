import { Component, OnDestroy, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { filter, map } from 'rxjs/operators';
import { ResponseWrapper } from '../../../shared';
import {ExperienceService} from '../../../entities/experience';

@Component({
    selector: 'jhi-experiences',
    templateUrl: './experience.component.html',
    styleUrls: ['./experience.component.css']
})
export class JhiExperiencesComponent implements OnInit, OnDestroy {

    options: any;
    autocompleteItems: any;
    queryListExperiences: any;
    experiencesValue = [];
    @Output() experiencesChange = new EventEmitter();

    @Input()
    get experiences() {
        return this.experiencesValue;
    }

    set experiences(val) {
        this.experiencesValue = val;
        this.experiencesChange.emit(this.experiencesValue);
    }

    constructor(private http: Http, private experienceService: ExperienceService) {
    }

    ngOnInit() {
        this.queryListExperiences = [];
        this.autocompleteItems = [];
        this.options = {
            placeholder: 'Añadir experiencía',
            secondaryPlaceholder: 'Añadir experiencía',
            maxItems: 10,
            autoComplete: true,
        }
    }

    ngOnDestroy() {
        this.experiences = [];
    }

    public requestAutocompleteItems = (text: string): Observable<Response> => {
        return this.experienceService.findByName(text);
    };

    public onAdding(experience): Observable<any> {
        return of(experience)
            .pipe();
    }

    public onRemoving(experience): Observable<any> {
        return of(experience)
            .pipe();
    }
}
