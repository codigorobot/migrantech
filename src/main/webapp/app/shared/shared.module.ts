import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    MigrantechSharedLibsModule,
    MigrantechSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    JhiLoginModalComponent,
    Principal,
    HasAnyAuthorityDirective,
} from './';
import {ProfileBlockComponent} from '../layouts/profileblock/profile-block.component';
import {AppMenuComponent, FiltersComponent, OperationsComponent} from '../layouts';
import {MigrantechAppMenuModule} from '../layouts/appmenu/app-menu.module';
import {MigrantechOperationsModule} from '../layouts/operations/operations.module';
import {MigrantechProfileBlockModule} from '../layouts/profileblock/profile-block.module';

@NgModule({
    imports: [
        MigrantechSharedLibsModule,
        MigrantechSharedCommonModule,
        MigrantechAppMenuModule,
        MigrantechOperationsModule,
        MigrantechProfileBlockModule
    ],
    declarations: [
        JhiLoginModalComponent,
        FiltersComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        MigrantechSharedCommonModule,
        JhiLoginModalComponent,
        ProfileBlockComponent,
        AppMenuComponent,
        OperationsComponent,
        ProfileBlockComponent,
        FiltersComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class MigrantechSharedModule {}
