import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { MigrantechSharedModule } from '../shared';
import { AgmCoreModule } from '@agm/core';
import { REPORTS_ROUTE, ReportsComponent } from './';
import { Ng2CompleterModule } from 'ng2-completer';
import { ChartModule } from 'angular-highcharts';

@NgModule({
    imports: [
        MigrantechSharedModule,
        RouterModule.forChild([ REPORTS_ROUTE ]),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC4F7DRotU1JniCKxzZBRwvt6UEsQ5rO0o'
         }),
        MyDatePickerModule,
        Ng2CompleterModule,
        ChartModule,
    ],
    declarations: [
        ReportsComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MigrantechReportsModule {}
