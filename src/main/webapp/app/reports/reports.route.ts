import { Route } from '@angular/router';

import { ReportsComponent } from './';

export const REPORTS_ROUTE: Route = {
    path: 'reports',
    component: ReportsComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
