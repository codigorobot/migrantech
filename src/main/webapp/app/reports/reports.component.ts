import { Component, OnInit, HostListener, Input, ElementRef, DoCheck, OnChanges, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiLanguageService, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { Account, LoginModalService, LOGIN_ALREADY_USED_TYPE, EMAIL_ALREADY_USED_TYPE } from '../shared';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../shared';
import {IMyDpOptions} from 'mydatepicker';
import { Node } from '../entities/node/node.model';
import { NodeService } from '../entities/node/node.service';
import { Place } from '../entities/place/place.model';
import { PlaceService } from '../entities/place/place.service';
import { CompleterCmp, CompleterData, CompleterService, CompleterItem, RemoteData } from 'ng2-completer';
import { Chart } from 'angular-highcharts';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
    selector: 'jhi-reports',
    templateUrl: './reports.component.html',
    styleUrls: [
        'reports.scss'
    ],
})
export class ReportsComponent implements OnInit, DoCheck {

    nodes: Node[];
    places: Place[];
    account: Account;
    centerMap: any = {  lat: 20.66, lng: -103.35};
    public myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'dd-mm-yyyy',
    };
    type: any = '';
    today: any = new Date();
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    search: any;
    @Input() municipe: any;
    dataRemoteUser: any;
    dataRemoteSubject: any;
    sumTotal: any;
     chart = new Chart({
          chart: {
            type: 'area'
          },
          title: {
            text: 'Comparativa'
          },
        xAxis: {
                categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo']
            },
          credits: {
            enabled: false
          },
         series: [{
            name: 'Me interesa',
            data: [200, 239, 345, 194, 56],
            color: '#3F51B5'
        }, {
            name: 'Vistas',
            data: [500, 345, 135, 239, 723],
            color: '#673AB7'
        }, {
            name: 'Comparticiones',
            data: [2, 367, 298, 145, 334],
            color: '#E91E63'
        },
         {
            name: 'Comentarios',
            data: [256, 167, 598, 345, 234],
            color: '#9C27B0'
        }]
        });

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private languageService: JhiLanguageService,
        private completerService: CompleterService,
        private nodeService: NodeService,
        private placeService: PlaceService,
        private jhiAlertService: JhiAlertService,
        private parseLinks: JhiParseLinks,
    ) {
      this.dataRemoteUser = completerService.remote(
            'login',
            'login',
            'login');

        this.dataRemoteUser.urlFormater( (term) => {
            return '/api/users/likeLogin/' + term;
        });
        this.nodes = [];
        this.places = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;

     }

    ngOnInit() {
        this.search = {
                type: '',
                dateEnd: {},
                dateStart: {},
                topic: '',
        };
        this.loadAll();
        this.sumTotal = { clikes: parseInt('1300', 10),
                            cviews: parseInt('2120', 10),
                            cshares: parseInt('5120', 10),
                            ccomments: parseInt('4234', 10) };
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }

    loadAll() {
        this.nodeService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        this.placeService.query({
               }).subscribe(
                (res: ResponseWrapper) => this.onSuccessPlaces(res.json, res.headers)
            );
         }

    public onSelectedUser(event) {

    }

    reset() {
        this.page = 0;
        this.nodes = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            this.nodes.push(data[i]);
        }
    }

    private onSuccessPlaces(data, headers) {
        let lat = 0;
        let lng = 0;
        for (let i = 0; i < data.length; i++) {

            const coord = data[i].location.replace('POINT (', '').replace(')', '').split(' ');
            lng = parseFloat(coord[1]);
            lat = parseFloat(coord[0]);
            data[i].lng = lng;
            data[i].lat = lat;
            this.places.push(data[i]);
        }
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    ngDoCheck() {
        if (this.municipe !== undefined && this.municipe !== {}) {

            if (this.municipe.geom !== undefined) {
            const point = this.municipe.geom.replace('POINT (', '').replace(')', '').split(' ');
            this.centerMap.lat =  parseFloat(point[1]);
            this.centerMap.lng =  parseFloat(point[0]);
            }else {
                this.centerMap = { lat: 20.66, lng: -103.35};
            }
        }
    }

    onChangeType(data) {
        this.sumTotal = { clikes: parseInt( Math.random() * 100 + '', 10),
                        cviews: parseInt( Math.random() * 100 + '', 10),
                        cshares: parseInt( Math.random() * 100 + '', 10),
                        ccomments: parseInt( Math.random() * 100 + '', 10) };

        const cont = this.chart.options.series.length;
         for (let i = 0; i < cont; i++) {
            this.chart.removeSerie(0);
         };
        console.log(this.chart);
        this.chart.addSerie({
              name: 'Me interesa',
            color: '#3F51B5',
              data: [
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
              ]
            });
        this.chart.addSerie({
              name: 'Visitas',
            color: '#673AB7',
              data: [
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
              ]
            });
        this.chart.addSerie({
              name: 'Comparticiones',
            color: '#E91E63',
              data: [
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
              ]
            });
        this.chart.addSerie({
              name: 'Comentearios',
            color: '#9C27B0',
              data: [
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
                Math.floor(Math.random() * 100),
              ]
            });
    }

    downloadCSV() {
        const csv = new Angular2Csv(this.nodes, 'report-filtered');
    }
}
