package com.coderobot.migrantech.repository;

import com.coderobot.migrantech.domain.MunicipiosInegi;
import com.coderobot.migrantech.domain.Node;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the Node entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MunicipiosInegiRepository extends JpaRepository<MunicipiosInegi, Long> {

    @Query("select distinct mun from MunicipiosInegi mun where lower(nom_ent) like lower(:name)")
    List<MunicipiosInegi> findStateByNameIsLike(@Param("name") String name);

    @Query("select distinct nomEnt from MunicipiosInegi order by nomEnt asc")
    List<MunicipiosInegi> getAllStates();

    @Query("select mun from MunicipiosInegi mun where lower(nom_mun) like lower(:name) and cve_ent = :cve")
    List<MunicipiosInegi> findMunByNameIsLike(@Param("name") String name, @Param("cve") String cve);

    @Query("select mun from MunicipiosInegi mun where gid = :gid")
    List<MunicipiosInegi> getByGid(@Param("gid") Long gid);


    //  List<String> getListMunicipios(@Param("name") String name,@Param("cve") Long cve);
}
