package com.coderobot.migrantech.repository;

import com.coderobot.migrantech.domain.Tag;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Tag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    List<Tag> findAllByTitleIsLike(String title);

    public Tag save(Tag tag);

}
