package com.coderobot.migrantech.repository;

import com.coderobot.migrantech.domain.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SponsorRepository extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {
}
