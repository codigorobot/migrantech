package com.coderobot.migrantech.repository;

import com.coderobot.migrantech.domain.Node;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TimelineRepository extends PagingAndSortingRepository<Node, Long>, JpaSpecificationExecutor<Node> {

}
