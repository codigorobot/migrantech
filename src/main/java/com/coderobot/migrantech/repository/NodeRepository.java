package com.coderobot.migrantech.repository;

import com.coderobot.migrantech.domain.Node;
import com.coderobot.migrantech.domain.enumeration.TypeNode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Node entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query("select distinct node from Node node left join fetch node.tags left join fetch node.places")
    List<Node> findAllWithEagerRelationships();

    @Query("select node from Node node left join fetch node.tags left join fetch node.places where node.id =:id")
    Node findOneWithEagerRelationships(@Param("id") Long id);

    Page<Node> findAllByTypeIsAndCreatedBy(Pageable pageable, TypeNode type, String user);
}
