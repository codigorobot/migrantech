/**
 * View Models used by Spring MVC REST controllers.
 */
package com.coderobot.migrantech.web.rest.vm;
