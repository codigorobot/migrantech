package com.coderobot.migrantech.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.coderobot.migrantech.domain.User;
import com.coderobot.migrantech.repository.SponsorRepository;
import com.coderobot.migrantech.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SponsorResource {

    private SponsorRepository sponsorRepository;

    public SponsorResource(SponsorRepository sponsorRepository) {
        this.sponsorRepository = sponsorRepository;
    }

    /**
     * GET  /sponsor/query : get all sponsors by query.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of messages in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/sponsor/query")
    @Timed
    public ResponseEntity<List<User>> gerAllSponsors(

        @ApiParam Pageable pageable
    )
        throws URISyntaxException {
        Specification<User> specification = new Specification<User>() {
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<Predicate>();
                predicates.add(builder.isTrue(root.get("showInList")));
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Page<User> page = sponsorRepository.findAll(specification,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sponsors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
