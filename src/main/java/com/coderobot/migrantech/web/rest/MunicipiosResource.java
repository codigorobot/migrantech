package com.coderobot.migrantech.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.json.*;
import java.util.ArrayList;
import java.net.*;
import java.util.*;
import java.io.InputStreamReader;
import java.io.InputStream;
import javax.transaction.Transactional;
import java.net.MalformedURLException;
import  org.springframework.web.util.UriComponentsBuilder;

/**
 * REST controller for getting Geoserver functions
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MunicipiosResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);


    public MunicipiosResource() {

    }


    /**
     * GET /municipios/agm get all states
     * @return String with results
     */
    @GetMapping("/municipios/agm")
    @Timed
    public String getStatesINEGI() {

       String result="";

        return result;
    }

}
