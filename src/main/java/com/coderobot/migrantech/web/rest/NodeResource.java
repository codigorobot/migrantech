package com.coderobot.migrantech.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.coderobot.migrantech.domain.MunicipiosInegi;
import com.coderobot.migrantech.domain.Node;

import com.coderobot.migrantech.domain.Place;
import com.coderobot.migrantech.domain.Tag;
import com.coderobot.migrantech.domain.enumeration.TypeNode;
import com.coderobot.migrantech.repository.MunicipiosInegiRepository;
import com.coderobot.migrantech.repository.NodeRepository;
import com.coderobot.migrantech.repository.PlaceRepository;
import com.coderobot.migrantech.repository.TagRepository;
import com.coderobot.migrantech.security.SecurityUtils;
import com.coderobot.migrantech.web.rest.errors.BadRequestAlertException;
import com.coderobot.migrantech.web.rest.util.HeaderUtil;
import com.coderobot.migrantech.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * REST controller for managing Node.
 */
@RestController
@RequestMapping("/api")
public class NodeResource {

    private final Logger log = LoggerFactory.getLogger(NodeResource.class);

    private static final String ENTITY_NAME = "node";

    private final NodeRepository nodeRepository;
    private  TagRepository tagRepository;
    private PlaceRepository placeRepository;
    private MunicipiosInegiRepository municipiosInegiRepository;

    public NodeResource(NodeRepository nodeRepository,
                        TagRepository tagRepository,
                        PlaceRepository placeRepository,
                        MunicipiosInegiRepository municipiosInegiRepository) {

        this.nodeRepository = nodeRepository;
        this.tagRepository = tagRepository;
        this.placeRepository = placeRepository;
        this.municipiosInegiRepository = municipiosInegiRepository;
    }

    /**
     * POST  /nodes : Create a new node.
     *
     * @param node the node to create
     * @return the ResponseEntity with status 201 (Created) and with body the new node, or with status 400 (Bad Request) if the node has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/nodes")
    @Timed
    public ResponseEntity<Node> createNode(@Valid @RequestBody Node node) throws URISyntaxException {
        log.debug("REST request to save Node : {}", node);
        Set<Tag> finalTags = new HashSet<Tag>();
        CopyOnWriteArrayList<Tag> tags2 = new CopyOnWriteArrayList<Tag>( node.getTags() );
        Iterator<Tag> iterator = tags2.iterator();

        Set<Place> finalPlaces = new HashSet<Place>();
        CopyOnWriteArrayList<Place> places2 = new CopyOnWriteArrayList<Place>(node.getPlaces());
        Iterator<Place> iteratorPlaces = places2.iterator();

        for (Place p : places2) {

            if(p.getId() == null) {
                Place place = this.placeRepository.save(p);
                finalPlaces.add(place);
            }else
                finalPlaces.add(p);
        }
        node.setPlaces(finalPlaces);

        for (Tag t : tags2) {
            if(t.getId()== null) {
                Tag tag = this.tagRepository.save(t);
                finalTags.add(tag);
            }else
                finalTags.add(t);
        }

        if (node.getId() != null) {
            throw new BadRequestAlertException("A new node cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Node result = nodeRepository.save(node);
        return ResponseEntity.created(new URI("/api/nodes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nodes : Updates an existing node.
     *
     * @param node the node to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated node,
     * or with status 400 (Bad Request) if the node is not valid,
     * or with status 500 (Internal Server Error) if the node couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/nodes")
    @Timed
    public ResponseEntity<Node> updateNode(@Valid @RequestBody Node node) throws URISyntaxException {
        log.debug("REST request to update Node : {}", node);

        Set<Tag> finalTags = new HashSet<Tag>();
        CopyOnWriteArrayList<Tag> tags2 = null;
        if (node.getTags() != null && node.getTags().size() > 0) {
            tags2 = new CopyOnWriteArrayList<Tag>( node.getTags() );
            Iterator<Tag> iterator = tags2.iterator();
            for (Tag t : tags2) {
                if(t.getId()== null) {
                    Tag tag = this.tagRepository.save(t);
                    finalTags.add(tag);
                }else
                    finalTags.add(t);
            }
        }

        Set<Place> finalPlaces = new HashSet<Place>();
        CopyOnWriteArrayList<Place> places2 = null;
        if (node.getPlaces() != null && node.getPlaces().size() > 0) {
            places2 = new CopyOnWriteArrayList<Place>(node.getPlaces());
            Iterator<Place> iteratorPlaces = places2.iterator();
            for (Place p : places2) {

                Place place = this.placeRepository.save(p);
                finalPlaces.add(place);

            }
            node.setPlaces(finalPlaces);
        }

        log.debug("DADADA {}",  places2);

        if (node.getId() == null) {
            return createNode(node);
        }
        Node result = nodeRepository.save(node);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, node.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nodes : get all the nodes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nodes in body
     */
    @GetMapping("/nodes")
    @Timed
    public ResponseEntity<List<Node>> getAllNodes(Pageable pageable) {
        log.debug("REST request to get a page of Nodes");
        Page<Node> page = nodeRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nodes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /nodes : get all the nodes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nodes in body
     */
    @GetMapping("/nodes/by-type/{type}")
    @Timed
    public ResponseEntity<List<Node>> getAllCurrentUserNodesByType(Pageable pageable,@PathVariable TypeNode type) {
        if (!SecurityUtils.getCurrentUserLogin().isPresent()) {
            return ResponseEntity.noContent().build();
        }
        String user = SecurityUtils.getCurrentUserLogin().get();
        Page<Node> page = nodeRepository.findAllByTypeIsAndCreatedBy(pageable,type,user);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nodes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /nodes/estados/:name : get all the nodes.
     *
     * @param name the name to search
     * @return the ResponseEntity with status 200 (OK) and the list of nodes in body
     */
    @GetMapping("/nodes/edo/{name}")
    @Timed
    public List<MunicipiosInegi> findEdoByName(@PathVariable String name) {

        log.debug("REST request to get States list");
        List<MunicipiosInegi> list = municipiosInegiRepository.findStateByNameIsLike(("%"+name+"%").toString());

        List<MunicipiosInegi> filterList = new ArrayList<MunicipiosInegi>();

        for(MunicipiosInegi mun:list){

            Boolean checkRepeted=false;
            for(MunicipiosInegi inner:filterList) {
                    if(mun.getCveEnt().toString().equals(inner.getCveEnt().toString()))
                            checkRepeted = true;
            }

            if(checkRepeted == false)
                filterList.add(mun);

        }
        return filterList;
    }


    /**
     * GET  /nodes/all-states : get all the nodes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of nodes in body
     */
    @GetMapping("/nodes/all-states")
    @Timed
    public List<MunicipiosInegi> getAllStates() {
        List<MunicipiosInegi> list = municipiosInegiRepository.getAllStates();
        return list;
    }


    /**
     * GET  /nodes/estados/:name/:cve : get all the nodes.
     *
     * @param name, the name to search
     * @param cve, the cve_edo to filter search
     * @return the ResponseEntity with status 200 (OK) and the list of nodes in body
     */
    @GetMapping("/nodes/mun/{name}/{cve}")
    @Timed
    public List<MunicipiosInegi> findMunByName(@PathVariable String name,@PathVariable String cve) {

        log.debug("REST request to get States list");
        List<MunicipiosInegi> list = municipiosInegiRepository.findMunByNameIsLike(("%"+name+"%").toString(),cve);
        return list;
    }


    /**
     * GET  /nodes/mun/:gid : get all the nodes.
     *
     * @param gid the gid to search
     * @return the ResponseEntity with status 200 (OK) and the list of nodes in body
     */
    @GetMapping("/nodes/mun/{gid}")
    @Timed
    public List<MunicipiosInegi> findMunByGid(@PathVariable Long gid) {

        log.debug("REST request to get States list");
        List<MunicipiosInegi> list = municipiosInegiRepository.getByGid(gid);
        return list;
    }


    /**
     * GET  /nodes/:id : get the "id" node.
     *
     * @param id the id of the node to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the node, or with status 404 (Not Found)
     */
    @GetMapping("/nodes/{id}")
    @Timed
    public ResponseEntity<Node> getNode(@PathVariable Long id) {
        log.debug("REST request to get Node : {}", id);
        Node node = nodeRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(node));
    }

    /**
     * DELETE  /nodes/:id : delete the "id" node.
     *
     * @param id the id of the node to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/nodes/{id}")
    @Timed
    public ResponseEntity<Void> deleteNode(@PathVariable Long id) {
        log.debug("REST request to delete Node : {}", id);
        nodeRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
