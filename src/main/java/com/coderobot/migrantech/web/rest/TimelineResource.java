package com.coderobot.migrantech.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.coderobot.migrantech.domain.Node;
import com.coderobot.migrantech.domain.enumeration.Topic;
import com.coderobot.migrantech.domain.enumeration.TypeNode;
import com.coderobot.migrantech.repository.TimelineRepository;
import com.coderobot.migrantech.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TimelineResource {

    private TimelineRepository timelineRepository;

    public TimelineResource(TimelineRepository timelineRepository) {
        this.timelineRepository = timelineRepository;
    }

    /**
     * GET  /timeline/query : get all messages by query.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of messages in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/timeline/query")
    @Timed
    public ResponseEntity<List<Node>> getAllMessagesByQuery(
        @RequestParam(value="nodeTypes",required=false) List<TypeNode> nodeTypes,
        @RequestParam(value="topics",required=false) List<Topic> topics,
        @RequestParam(value="tags",required=false) List<Long> tags,
        @RequestParam(value="states",required=false) List<String> states,
        @RequestParam(value="users",required=false) List<String> users,
        @RequestParam(value="search",required=false) String search,
        @ApiParam Pageable pageable
    )
        throws URISyntaxException {
        Specification<Node> specification = new Specification<Node>() {
            public Predicate toPredicate(Root<Node> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<Predicate>();
                if (nodeTypes != null && nodeTypes.size() > 0) predicates.add(root.get("type").in(nodeTypes));
                if (topics != null && topics.size() > 0) predicates.add(root.get("topic").in(topics));
                if (tags != null && tags.size() > 0) predicates.add(root.join("tags").get("id").in(tags));
                // State predicates
                if (states != null && states.size() > 0) {
                    List<Predicate> statePredicate = new ArrayList<Predicate>();
                    for (String state:states) {
                        statePredicate.add(builder.like(root.get("influenceArea"), "%"+state+"%"));
                    }
                    predicates.add(builder.or(statePredicate.toArray(new Predicate[statePredicate.size()])));
                }
                if (users != null && users.size() > 0) predicates.add(root.get("createdBy").in(users));

                // Search text
                if (search != null && search.length() > 0) {
                    String[] terms = search.split("\\s+");
                    List<Predicate> textSearchPredicates = new ArrayList<Predicate>();
                    List<Predicate> statePredicate = new ArrayList<Predicate>();
                    for (String term: terms) {
                        statePredicate.add(builder.like(builder.lower(root.get("influenceArea")), "%"+term.toLowerCase()+"%"));
                    }
                    List<Predicate> statePredicate1 = new ArrayList<Predicate>();
                    for (String term: terms) {
                        statePredicate1.add(builder.like(builder.lower(root.get("body")), "%"+term.toLowerCase()+"%"));
                    }
                    List<Predicate> statePredicate2 = new ArrayList<Predicate>();
                    for (String term: terms) {
                        statePredicate2.add(builder.like(builder.lower(root.get("title")), "%"+term.toLowerCase()+"%"));
                    }

                    textSearchPredicates.add(builder.and(statePredicate.toArray(new Predicate[statePredicate.size()])));
                    textSearchPredicates.add(builder.and(statePredicate1.toArray(new Predicate[statePredicate1.size()])));
                    textSearchPredicates.add(builder.and(statePredicate2.toArray(new Predicate[statePredicate2.size()])));
                    predicates.add(builder.or(textSearchPredicates.toArray(new Predicate[textSearchPredicates.size()])));
                }

                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
        Page<Node> page = timelineRepository.findAll(specification,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/nodes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
