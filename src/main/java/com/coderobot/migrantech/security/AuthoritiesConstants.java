package com.coderobot.migrantech.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String CIUDADANO = "ROLE_CIUDADANO";

    public static final String MIGRANTE = "ROLE_MIGRANTE";

    public static final String COLABORADOR = "ROLE_COLABORADOR";

    private AuthoritiesConstants() {
    }
}
