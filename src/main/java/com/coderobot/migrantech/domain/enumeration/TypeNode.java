package com.coderobot.migrantech.domain.enumeration;

/**
 * The TypeNode enumeration.
 */
public enum TypeNode {
    Pregunta, Blog, ProgramaApoyo, Notas, BolsaTrabajo, Eventos, Convocatoria, OficinaAtencion
}
