package com.coderobot.migrantech.domain.enumeration;

/**
 * The Topic enumeration.
 */
public enum Topic {
    Educacion, Trabajo, Emprendurismo
}
