package com.coderobot.migrantech.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * A Municipio
 */
@Entity
@Table(name = "municipios_inegi")
public class MunicipiosInegi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long gid;

    @Column(name = "cve_ent")
    private String cveEnt;

    @Column(name = "nom_ent")
    private String nomEnt;

    @Column(name = "cve_mun")
    private String cveMun;

    @Column(name = "nom_mun")
    private String nomMun;

    @Column(name = "edomun")
    private String edomun;

    @Column(columnDefinition = "geometry(MultiPolygon,4326)")
    private MultiPolygon geom;


    public Long getGid() { return this.gid; }
    public void setGid(Long gid) { this.gid = gid; }

    public String getCveEnt() { return this.cveEnt; }
    public void setCveEnt(String cveEnt) { this.cveEnt = cveEnt; }

    public String getNomEnt() { return this.nomEnt; }
    public void setNomEnt(String nomEnt) { this.nomEnt = nomEnt; }

    public String getCveMun() { return this.cveMun; }
    public void setCveMun(String cveMun) { this.cveMun = cveMun; }

    public String getNomMun() { return this.nomMun; }
    public void setNomMun(String nomMun) { this.nomMun = nomMun; }

    public String getEdomun() { return this.edomun; }
    public void setEdomun(String edomun) { this.edomun = edomun; }

    public String getGeom() {
        if(geom!=null)
            return geom.getCentroid().toText();
        return null;
    }
    public void setGeom(String geom){
        if(geom!=null && geom.length() > 0 ) {
            GeometryFactory geometryFactory = new GeometryFactory();
            WKTReader reader = new WKTReader(geometryFactory);
            try {
                this.geom = (MultiPolygon) reader.read(geom);
                this.geom.setSRID(4326);
                this.geom.getCentroid();
            } catch (ParseException e) {
                System.out.println("Geom exception");
                System.out.println(e.getLocalizedMessage());
            }
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MunicipiosInegi mun = (MunicipiosInegi) o;
        if (mun.getGid() == null || getGid() == null) {
            return false;
        }
        return Objects.equals(getGid(), mun.getGid());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getGid());
    }

    @Override
    public String toString() {
        return "MunicipiosInegi{" +
            "id=" + getGid() +
            ", cveEnt='" + getCveEnt() + "'" +
            ", nomEnt='" + getNomEnt() + "'" +
            ", cveMun='" + getCveMun() + "'" +
            ", nomMnun='" + getNomMun() + "'" +
            ", edomun='" + getEdomun() + "'" +
            "}";
    }
}
