package com.coderobot.migrantech.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.coderobot.migrantech.domain.enumeration.Topic;

import com.coderobot.migrantech.domain.enumeration.TypeNode;


/**
 * A Node.
 */
@Entity
@Table(name = "node")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Node extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "jhi_body", nullable = false)
    private String body;

    @NotNull
    @Column(name = "influence_area", nullable = false)
    private String influenceArea;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "topic", nullable = false)
    private Topic topic;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private TypeNode type;

    @Lob
    @Column(name = "jhi_file")
    private byte[] file;

    @Column(name = "jhi_file_content_type")
    private String fileContentType;

    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "node_tags",
               joinColumns = @JoinColumn(name="nodes_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="tags_id", referencedColumnName="id"))
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "node_place",
               joinColumns = @JoinColumn(name="nodes_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="places_id", referencedColumnName="id"))
    private Set<Place> places = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Node title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public Node body(String body) {
        this.body = body;
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getInfluenceArea() {
        return influenceArea;
    }

    public Node influenceArea(String influenceArea) {
        this.influenceArea = influenceArea;
        return this;
    }

    public void setInfluenceArea(String influenceArea) {
        this.influenceArea = influenceArea;
    }

    public Topic getTopic() {
        return topic;
    }

    public Node topic(Topic topic) {
        this.topic = topic;
        return this;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public TypeNode getType() {
        return type;
    }

    public Node type(TypeNode type) {
        this.type = type;
        return this;
    }

    public void setType(TypeNode type) {
        this.type = type;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public Node tags(Set<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public Node addTags(Tag tag) {
        this.tags.add(tag);
        tag.getNodes().add(this);
        return this;
    }

    public Node removeTags(Tag tag) {
        this.tags.remove(tag);
        tag.getNodes().remove(this);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Place> getPlaces() {
        return places;
    }

    public Node places(Set<Place> places) {
        this.places = places;
        return this;
    }

    public Node addPlace(Place place) {
        this.places.add(place);
        place.getNodes().add(this);
        return this;
    }

    public Node removePlace(Place place) {
        this.places.remove(place);
        place.getNodes().remove(this);
        return this;
    }

    public void setPlaces(Set<Place> places) {
        this.places = places;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public byte[] getFile() {
        return file;
    }

    public Node file(byte[] file) {
        this.file = file;
        return this;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public Node fileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
        return this;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Node node = (Node) o;
        if (node.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), node.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Node{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", body='" + getBody() + "'" +
            ", influenceArea='" + getInfluenceArea() + "'" +
            ", topic='" + getTopic() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
