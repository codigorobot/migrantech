SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE TABLE municipios_inegi (
    gid integer NOT NULL,
    cve_ent character varying(2),
    nom_ent character varying(254),
    cve_mun character varying(3),
    nom_mun character varying(254),
    edomun  character varying(6),
    geom geometry(MultiPolygon,4326)
);


ALTER TABLE municipios_inegi OWNER TO postgres;

CREATE SEQUENCE municipios_inegi_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE municipios_inegi_gid_seq OWNED BY municipios_inegi.gid;

ALTER TABLE ONLY municipios_inegi ALTER COLUMN gid SET DEFAULT nextval('municipios_inegi_gid_seq'::regclass);

ALTER TABLE ONLY municipios_inegi
    ADD CONSTRAINT municipios_inegi_pkey PRIMARY KEY (gid);
